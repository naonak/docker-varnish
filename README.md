# Varnish Docker container

> Debian 8
> Varnish >= 5.x
> It will still maintained with different versions Varnish along time and theirs tags

## Reasons to upgrade to Varnish 5.0
The separate VCL/VCL labels feature allow one vcl file by domain/host and can probably help you untangle your VCL code if it has become too complex. Upgrading from 4.1 to get that feature should be a no-brainer.

https://varnish-cache.org/docs/5.0/whats-new/relnote-5.0.html
https://varnish-cache.org/docs/5.0/whats-new/upgrading-5.0.html#whatsnew-upgrading-5-0


## Usage

To use this container, you will need to provide your custom config.vcl (which is usually the case).

```
docker run -d \
  --link web-app:backend-host \
  --volumes-from web-app \
  --env 'VCL_CONFIG=/data/path/to/varnish.vcl' \
  naonak/varnish
```

In the above example we assume that:
* You have your application running inside `web-app` container and web server there is running on port 80 (although you don't need to expose that port, as we use --link and varnish will connect directly to it)
* `web-app` container has `/data` volume with `varnish.vcl` somewhere there
* `web-app` is aliased inside varnish container as `backend-host`
* Your `varnish.vcl` should contain at least backend definition like this:
```
backend default {
    .host = "backend-host";
    .port = "80";
}
```
or for separate vcl files, look at https://varnish-cache.org/docs/trunk/users-guide/vcl-separate.html

## Environmental variables

You can configure Varnish daemon by following env variables:

> **VCL_CONFIG** `/etc/varnish/default.vcl`
> **CACHE_SIZE** `64m`
> **VARNISHD_PARAMS** `-p default_ttl=3600 -p default_grace=3600`


## Author(s)

* Fabien Even
* Initialy based on https://hub.docker.com/r/million12/varnish/